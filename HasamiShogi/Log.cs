﻿using System;

namespace HasamiShogi {
    /// <summary>
    /// ログを管理
    /// </summary>
    public class Log {
        /// <summary>
        /// プレイヤー1の得点ログ
        /// </summary>
        public string Player1PointLog { get; private set; }
        /// <summary>
        /// プレイヤー2の得点ログ
        /// </summary>
        public string Player2PointLog { get; private set; }
        /// <summary>
        /// プレイヤー1の行動ログ
        /// </summary>
        public string Player1ActionLog { get; private set; }
        /// <summary>
        /// プレイヤー2の行動ログ
        /// </summary>
        public string Player2ActionLog { get; private set; }
        /// <summary>
        /// 手番になっているプレイヤーのログ
        /// </summary>
        public string TurnPlayerLog { get; set; }
        /// <summary>
        /// ターン数のログ
        /// </summary>
        public int TurnCountLog { get; set; }
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="vPlayerName"></param>
        public Log(string vPlayerName) => InitializeLog(vPlayerName);
        /// <summary>
        /// ログの初期化
        /// </summary>
        /// <param name="vPlayerName"></param>
        public void InitializeLog(string vPlayerName) {
            this.Player1PointLog = "0";
            this.Player2PointLog = "0";
            this.Player1ActionLog = "";
            this.Player2ActionLog = "";
            this.TurnCountLog = 0;
            this.TurnPlayerLog = vPlayerName;
        }
        /// <summary>
        /// 得点ログの作成
        /// </summary>
        /// <param name="vPoint">得点</param>
        /// <param name="vPlayerKind">現在のプレイヤーの種類</param>
        public void MakePointLog(int vPoint, PlayerKindEnum vPlayerKind) {
            switch (vPlayerKind) {
                case PlayerKindEnum.Player1:
                    this.Player1PointLog = vPoint.ToString();
                    break;
                case PlayerKindEnum.Player2:
                    this.Player2PointLog = vPoint.ToString();
                    break;
                default: break;
            }
        }
        /// <summary>
        /// 移動ログの作成
        /// </summary>
        /// <param name="vCurrentIndex">移動元のインデックス</param>
        /// <param name="vTargetIndex">移動先のインデックス</param>
        /// <param name="vPlayerKind">現在のプレイヤーの種類</param>
        public void MakeMoveLog((int X, int Y) vCurrentIndex, (int X, int Y) vTargetIndex, PlayerKindEnum vPlayerKind) {
            var wCurrentIndex = (vCurrentIndex.X + 1, vCurrentIndex.Y + 1);
            var wTargetIndex = (vTargetIndex.X + 1, vTargetIndex.Y + 1);
            switch (vPlayerKind) {
                case PlayerKindEnum.Player1:
                    this.Player1ActionLog = $"{TurnCountLog + 1}：{wCurrentIndex}から{wTargetIndex}へ移動{Environment.NewLine}{this.Player1ActionLog}";
                    break;
                case PlayerKindEnum.Player2:
                    this.Player2ActionLog = $"{TurnCountLog + 1}：{wCurrentIndex}から{wTargetIndex}へ移動{Environment.NewLine}{this.Player2ActionLog}";
                    break;
                default: break;
            }
        }
        /// <summary>
        /// 取り除くログの作成
        /// </summary>
        /// <param name="vPlayerKind">現在のプレイヤーの種類</param>
        public void MakeRemoveLog((int X,int Y) vPieceIndex,PlayerKindEnum vPlayerKind) {
            var wPieceIndex = (vPieceIndex.X + 1, vPieceIndex.Y + 1);
            switch (vPlayerKind) {
                case PlayerKindEnum.Player1:
                    this.Player1ActionLog = $"{TurnCountLog + 1}：駒{wPieceIndex}が取られた{Environment.NewLine}{this.Player1ActionLog}";
                    break;
                case PlayerKindEnum.Player2:
                    this.Player2ActionLog = $"{TurnCountLog + 1}：駒{wPieceIndex}が取られた{Environment.NewLine}{this.Player2ActionLog}";
                    break;
                default: break;
            }
        }
    }
}
