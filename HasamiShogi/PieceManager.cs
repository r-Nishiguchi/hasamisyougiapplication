﻿using System;
using System.Collections.Generic;

namespace HasamiShogi {
    /// <summary>
    /// 配列の駒を管理
    /// </summary>
    public class PieceManager {
        /// <summary>
        /// 盤面の配列
        /// </summary>
        public Piece[][] Board { get; private set; } = new Piece[Define.C_FieldLength][];
        /// <summary>
        /// 現在選択されている駒
        /// </summary>
        public Piece CurrentPiece { get; set; }
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public PieceManager() {
            for (int i = 0; i < Define.C_FieldLength; i++) {
                this.Board[i] = new Piece[Define.C_FieldLength];
            }
        }
        /// <summary>
        /// 配列の初期化
        /// </summary>
        /// <param name="vPlayer1">プレイヤー1</param>
        /// <param name="vPlayer2">プレイヤー2</param>
        public void InitializeBoard(Player vPlayer1, Player vPlayer2) {
            for (int i = 0; i < Define.C_FieldLength; i++) {
                for (int j = 0; j < Define.C_FieldLength; j++) {
                    Piece wPiece = this.Board[i][j];
                    wPiece.Enabled = true;
                    if (j == Define.C_FieldLength - 1) {
                        wPiece.InitializePiece(vPlayer1);
                    } else if (j == 0) {
                        wPiece.InitializePiece(vPlayer2);
                    } else {
                        wPiece.InitializePiece(null);
                    }
                }
            }
            this.CurrentPiece = null;
        }
        /// <summary>
        /// 移動可能な場所を取得
        /// </summary>
        /// <param name="vPiece">選択された駒</param>
        /// <returns></returns>
        public List<Piece> GetMovableArea(Piece vPiece) {
            List<Piece> wMovablePiecies = new List<Piece>();
            //4方向を探索
            foreach (DirectionKindEnum wDirection in Enum.GetValues(typeof(DirectionKindEnum))) {
                Piece wMovableButton = GetNextPiece(wDirection, vPiece);
                //壁でない限り続ける
                while (wMovableButton != null) {
                    //プレイヤーが持つ駒が現れたら終了
                    if (wMovableButton.PlayerKind != null) break;
                    wMovablePiecies.Add(wMovableButton);
                    wMovableButton = GetNextPiece(wDirection, wMovableButton);
                }
            }
            return wMovablePiecies;
        }
        /// <summary>
        /// 駒を入れ替える
        /// </summary>
        /// <param name="vTargetPiece">移動先の駒</param>
        public ValueTuple<(int,int),(int,int)> SwitchPiece(Piece vTargetPiece) {
            (int X, int Y) wCurrentIndex = GetIndex(this.CurrentPiece);
            (int X, int Y) wTargetIndex = GetIndex(vTargetPiece);

            this.Board[wCurrentIndex.Y][wCurrentIndex.X] = vTargetPiece;
            this.Board[wTargetIndex.Y][wTargetIndex.X] = this.CurrentPiece;
            return (wCurrentIndex,wTargetIndex);
        }
        /// <summary>
        /// 駒が入っている配列のインデックスを取得
        /// </summary>
        /// <param name="vPiece">選択された駒</param>
        /// <returns></returns>
        public (int X, int Y) GetIndex(Piece vPiece) {
            //選択されたボタンを見つけてbreak
            for (int i = 0; i < Define.C_FieldLength; i++) {
                for (int j = 0; j < Define.C_FieldLength; j++) {
                    if (this.Board[i][j] != vPiece) continue;
                    return (j, i);
                }
            }
            return (-1, -1);
        }
        /// <summary>
        /// 隣の駒を取得する
        /// </summary>
        /// <param name="vDirection">方向</param>
        /// <param name="vPiece">対象の駒</param>
        /// <returns></returns>
        private Piece GetNextPiece(DirectionKindEnum vDirection, Piece vPiece) {
            (int wIndexX, int wIndexY) = GetIndex(vPiece);
            //方向を確認し、１つ進めて壁ならnullを返す。それ以外はボタンを返す
            switch (vDirection) {
                case DirectionKindEnum.Up: return (wIndexY == 0) ? null : this.Board[wIndexY - 1][wIndexX];
                case DirectionKindEnum.Right: return (wIndexX == Define.C_FieldLength - 1) ? null : this.Board[wIndexY][wIndexX + 1];
                case DirectionKindEnum.Down: return (wIndexY == Define.C_FieldLength - 1) ? null : this.Board[wIndexY + 1][wIndexX];
                case DirectionKindEnum.Left: return (wIndexX == 0) ? null : this.Board[wIndexY][wIndexX - 1];
                default: return null;
            }
        }
        /// <summary>
        /// 壁に追い込んで取り除けるかどうか
        /// </summary>
        /// <param name="vDirection"></param>
        /// <returns></returns>
        public bool IsGetRemoveByWall(DirectionKindEnum vDirection) {
            Piece wRemovePiece = GetNextPiece(vDirection, this.CurrentPiece);
            if (wRemovePiece != null) {
                if (wRemovePiece.PlayerKind != null) {
                    if (IsSetRemovableFlagByWall(wRemovePiece)) return true;
                }
            }
            return false;
        }
        /// <summary>
        /// 取り除けるかどうか
        /// </summary>
        /// <param name="vDirection"></param>
        /// <returns></returns>
        public bool IsGetRemove(DirectionKindEnum vDirection) {
            Piece wRemovePiece = GetNextPiece(vDirection, this.CurrentPiece);
            while (wRemovePiece != null) {
                //プレイヤーがいない駒ならば終了
                if (wRemovePiece.PlayerKind == null) break;
                //最初の駒と同じ種類の駒が来たら終了
                if (wRemovePiece.PlayerKind == this.CurrentPiece.PlayerKind) return true;
                wRemovePiece.IsRemovable = true;
                wRemovePiece = GetNextPiece(vDirection, wRemovePiece);
            }
            return false;
        }
        /// <summary>
        /// 追い込んで取り除くフラグをセットしているか
        /// </summary>
        /// <param name="vPiece"></param>
        /// <returns></returns>
        private bool IsSetRemovableFlagByWall(Piece vPiece) {
            //4方向を探索
            foreach (DirectionKindEnum wDirection in Enum.GetValues(typeof(DirectionKindEnum))) {
                Piece wRemovePiece = GetNextPiece(wDirection, vPiece);
                //壁もしくは、現在のプレイヤーが持つ持ち駒だったら、もしくは駒がすでにtrueなら次へ進む
                if (wRemovePiece == null || wRemovePiece.PlayerKind == this.CurrentPiece.PlayerKind || wRemovePiece.IsRemovable) continue;
                //プレイヤーがいない駒だったらfalseで終了
                if (wRemovePiece.PlayerKind == null) return false;
                wRemovePiece.IsRemovable = true;
                //次の駒で再度4方向を探索
                if (!IsSetRemovableFlagByWall(wRemovePiece)) return false;
            }
            vPiece.IsRemovable = true;
            return true;
        }
    }
}
