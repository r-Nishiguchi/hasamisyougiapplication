﻿using System;
using System.Windows.Forms;

namespace HasamiShogi {
    /// <summary>
    /// ルール画面
    /// </summary>
    public partial class RuleForm : Form {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public RuleForm() {
            InitializeComponent();
        }
        /// <summary>
        /// 閉じるボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
