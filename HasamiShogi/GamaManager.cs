﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace HasamiShogi {
    /// <summary>
    /// ゲームを管理
    /// </summary>
    public class GamaManager {
        private PieceManager FPieceManager;
        private PlayerManager FPlayerManager;
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public GamaManager() {
            var wPlayer1 = new Player("プレイヤー1", PlayerKindEnum.Player1, Color.Black, "歩");
            var wPlayer2 = new Player("プレイヤー2", PlayerKindEnum.Player2, Color.Red, "と");
            FPlayerManager = new PlayerManager(wPlayer1, wPlayer2);
            FPieceManager = new PieceManager();
        }
        /// <summary>
        /// 初期化
        /// </summary>
        public void Initialize() => FPieceManager.InitializeBoard(FPlayerManager.CurrentPlayer, FPlayerManager.AnotherPlayer);
        /// <summary>
        /// ボードへ追加
        /// </summary>
        /// <param name="vPiece">追加する駒</param>
        /// <param name="vIndex">インデックス</param>
        public void AddBoard(Piece vPiece, (int X, int Y) vIndex) => FPieceManager.Board[vIndex.Y][vIndex.X] = vPiece;
        /// <summary>
        /// 駒の選択
        /// </summary>
        /// <param name="vPiece">クリックした駒</param>
        public void SelectPiece(Piece vPiece) {
            //現在手番のプレイヤーの駒出なかったら終了
            if (vPiece.PlayerKind != FPlayerManager.CurrentPlayer.Kind) return;
            ChangeColor(vPiece);
            //同じ駒を選択したら終了
            if (FPieceManager.CurrentPiece == vPiece) {
                FPieceManager.CurrentPiece = null;
                return;
            }
            //すでに選択中の駒があれば、色を戻す
            if (FPieceManager.CurrentPiece != null) ChangeColor(FPieceManager.CurrentPiece);

            FPieceManager.CurrentPiece = vPiece;
        }
        /// <summary>
        /// 色を変える
        /// </summary>
        /// <param name="vPiece">色を変える駒</param>
        private void ChangeColor(Piece vPiece) {
            //プロパティを変更すると色が変わる
            vPiece.IsSelected = !vPiece.IsSelected;
            FPieceManager.GetMovableArea(vPiece).ForEach(n => n.IsMovavle = !n.IsMovavle);
        }
        /// <summary>
        /// 駒の移動
        /// </summary>
        /// <param name="vTargetPiece">移動先の駒</param>
        public void MovePiece(Piece vTargetPiece) {
            bool wIsThrrePointGap = FPlayerManager.IsThrrePointGap;
            if (FPieceManager.CurrentPiece == null || !FPieceManager.GetMovableArea(FPieceManager.CurrentPiece).Contains(vTargetPiece)) return;
            ChangeColor(FPieceManager.CurrentPiece);
            //配列の駒を入れ替え
            var wIndexLog = FPieceManager.SwitchPiece(vTargetPiece);
            FPlayerManager.PlayerLog.MakeMoveLog(wIndexLog.Item1,wIndexLog.Item2,FPlayerManager.CurrentPlayer.Kind);
            //盤面の駒を入れ替え
            Point wCurrentLocation = FPieceManager.CurrentPiece.Location;
            FPieceManager.CurrentPiece.Location = vTargetPiece.Location;
            vTargetPiece.Location = wCurrentLocation;

            RemovePiece();
            ChangeTurn();
            if (FPlayerManager.IsEnd(wIsThrrePointGap)) ShowResult(FPlayerManager.GetWinner());
        }
        /// <summary>
        /// 駒を取り除く
        /// </summary>
        public void RemovePiece() {
            foreach (DirectionKindEnum wDirection in Enum.GetValues(typeof(DirectionKindEnum))) {
                //追い詰めた処理
                if (FPieceManager.IsGetRemoveByWall(wDirection)) CountPoint();
                FPlayerManager.AnotherPlayer.MyPiecies.ForEach(n => n.IsRemovable = false);
                //挟んだ処理
                if (FPieceManager.IsGetRemove(wDirection)) CountPoint();
                FPlayerManager.AnotherPlayer.MyPiecies.ForEach(n => n.IsRemovable = false);
            }
        }
        /// <summary>
        /// 点数を数える
        /// </summary>
        private void CountPoint() {
            //持ち駒で取り除く判定になっている数を加算する
            FPlayerManager.CurrentPlayer.Point += FPlayerManager.AnotherPlayer.MyPiecies.Count(n => n.IsRemovable);
            FPlayerManager.PlayerLog.MakePointLog(FPlayerManager.CurrentPlayer.Point, FPlayerManager.CurrentPlayer.Kind);

            //取り除く駒の状態を空のボタンに変更する
            foreach (Piece wPiece in FPlayerManager.AnotherPlayer.MyPiecies.Where(n => n.IsRemovable)) {
                wPiece.InitializePiece(null);
                FPlayerManager.PlayerLog.MakeRemoveLog(FPieceManager.GetIndex(wPiece),FPlayerManager.AnotherPlayer.Kind);
            }
            //プレイヤーがいない駒を取り除いて終了
            FPlayerManager.AnotherPlayer.MyPiecies.RemoveAll(n => n.PlayerKind == null);
        }
        /// <summary>
        /// 手番を切り替える
        /// </summary>
        private void ChangeTurn() {
            //AnotherとCurrentのプレイヤーを入れ替え
            FPlayerManager.SwitchPlayer();
            FPieceManager.CurrentPiece = null;
        }
        /// <summary>
        /// 結果を表示
        /// </summary>
        /// <param name="vWinner"></param>
        private void ShowResult(Player vWinner) {
            MessageBox.Show($"{vWinner.Name}の勝ちです");
            foreach (Piece[] wPiecies in FPieceManager.Board) {
                foreach (Piece wPiece in wPiecies) {
                    wPiece.Enabled = false;
                }
            }
        }
        /// <summary>
        /// プレイヤーのログを取得
        /// </summary>
        /// <returns></returns>
        public Log GetPlayerLog() => FPlayerManager.PlayerLog;
        /// <summary>
        /// ゲームをリセット
        /// </summary>
        public void ResetGame() {
            if (FPlayerManager.CurrentPlayer.Kind == PlayerKindEnum.Player1)FPlayerManager.InitializePlayerManager(FPlayerManager.CurrentPlayer, FPlayerManager.AnotherPlayer);
            else FPlayerManager.InitializePlayerManager(FPlayerManager.AnotherPlayer, FPlayerManager.CurrentPlayer);
            FPlayerManager.CurrentPlayer.Initialize();
            FPlayerManager.AnotherPlayer.Initialize();
            FPieceManager.InitializeBoard(FPlayerManager.CurrentPlayer, FPlayerManager.AnotherPlayer);
            FPlayerManager.PlayerLog.InitializeLog(FPlayerManager.CurrentPlayer.Name);

        }
    }
}
