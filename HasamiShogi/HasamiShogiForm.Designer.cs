﻿namespace HasamiShogi {
    partial class HasamiShogiForm {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent() {
            this.FSplitContainer1 = new System.Windows.Forms.SplitContainer();
            this.FRuleButton = new System.Windows.Forms.Button();
            this.FResetButton = new System.Windows.Forms.Button();
            this.FLabel6 = new System.Windows.Forms.Label();
            this.FTurnCountTextBox = new System.Windows.Forms.TextBox();
            this.Flabel5 = new System.Windows.Forms.Label();
            this.Flabel4 = new System.Windows.Forms.Label();
            this.FPlayer1RichTextBox = new System.Windows.Forms.RichTextBox();
            this.FPlayer2RichTextBox = new System.Windows.Forms.RichTextBox();
            this.FLabel3 = new System.Windows.Forms.Label();
            this.FLabel2 = new System.Windows.Forms.Label();
            this.FLabel1 = new System.Windows.Forms.Label();
            this.FTurnTextBox = new System.Windows.Forms.TextBox();
            this.FPlayer2PointTextBox = new System.Windows.Forms.TextBox();
            this.FPlayer1PointTextBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.FSplitContainer1)).BeginInit();
            this.FSplitContainer1.Panel2.SuspendLayout();
            this.FSplitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // FSplitContainer1
            // 
            this.FSplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FSplitContainer1.IsSplitterFixed = true;
            this.FSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.FSplitContainer1.Name = "FSplitContainer1";
            // 
            // FSplitContainer1.Panel1
            // 
            this.FSplitContainer1.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.FSplitContainer1.Panel1.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            // 
            // FSplitContainer1.Panel2
            // 
            this.FSplitContainer1.Panel2.Controls.Add(this.FRuleButton);
            this.FSplitContainer1.Panel2.Controls.Add(this.FResetButton);
            this.FSplitContainer1.Panel2.Controls.Add(this.FLabel6);
            this.FSplitContainer1.Panel2.Controls.Add(this.FTurnCountTextBox);
            this.FSplitContainer1.Panel2.Controls.Add(this.Flabel5);
            this.FSplitContainer1.Panel2.Controls.Add(this.Flabel4);
            this.FSplitContainer1.Panel2.Controls.Add(this.FPlayer1RichTextBox);
            this.FSplitContainer1.Panel2.Controls.Add(this.FPlayer2RichTextBox);
            this.FSplitContainer1.Panel2.Controls.Add(this.FLabel3);
            this.FSplitContainer1.Panel2.Controls.Add(this.FLabel2);
            this.FSplitContainer1.Panel2.Controls.Add(this.FLabel1);
            this.FSplitContainer1.Panel2.Controls.Add(this.FTurnTextBox);
            this.FSplitContainer1.Panel2.Controls.Add(this.FPlayer2PointTextBox);
            this.FSplitContainer1.Panel2.Controls.Add(this.FPlayer1PointTextBox);
            this.FSplitContainer1.Size = new System.Drawing.Size(810, 573);
            this.FSplitContainer1.SplitterDistance = 536;
            this.FSplitContainer1.TabIndex = 0;
            // 
            // FRuleButton
            // 
            this.FRuleButton.ForeColor = System.Drawing.Color.Black;
            this.FRuleButton.Location = new System.Drawing.Point(132, 10);
            this.FRuleButton.Name = "FRuleButton";
            this.FRuleButton.Size = new System.Drawing.Size(59, 28);
            this.FRuleButton.TabIndex = 12;
            this.FRuleButton.Text = "ルール";
            this.FRuleButton.UseVisualStyleBackColor = true;
            this.FRuleButton.Click += new System.EventHandler(this.FRuleButton_Click);
            // 
            // FResetButton
            // 
            this.FResetButton.Location = new System.Drawing.Point(197, 10);
            this.FResetButton.Name = "FResetButton";
            this.FResetButton.Size = new System.Drawing.Size(59, 28);
            this.FResetButton.TabIndex = 13;
            this.FResetButton.Text = "リセット";
            this.FResetButton.UseVisualStyleBackColor = true;
            this.FResetButton.Click += new System.EventHandler(this.FResetButton_Click);
            // 
            // FLabel6
            // 
            this.FLabel6.AutoSize = true;
            this.FLabel6.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FLabel6.Location = new System.Drawing.Point(205, 52);
            this.FLabel6.Name = "FLabel6";
            this.FLabel6.Size = new System.Drawing.Size(60, 16);
            this.FLabel6.TabIndex = 11;
            this.FLabel6.Text = "ターン数";
            // 
            // FTurnCountTextBox
            // 
            this.FTurnCountTextBox.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FTurnCountTextBox.Location = new System.Drawing.Point(206, 71);
            this.FTurnCountTextBox.Name = "FTurnCountTextBox";
            this.FTurnCountTextBox.ReadOnly = true;
            this.FTurnCountTextBox.Size = new System.Drawing.Size(51, 23);
            this.FTurnCountTextBox.TabIndex = 10;
            this.FTurnCountTextBox.TabStop = false;
            // 
            // Flabel5
            // 
            this.Flabel5.AutoSize = true;
            this.Flabel5.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Flabel5.Location = new System.Drawing.Point(67, 321);
            this.Flabel5.Name = "Flabel5";
            this.Flabel5.Size = new System.Drawing.Size(115, 16);
            this.Flabel5.TabIndex = 9;
            this.Flabel5.Text = "プレイヤー1のログ";
            // 
            // Flabel4
            // 
            this.Flabel4.AutoSize = true;
            this.Flabel4.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Flabel4.Location = new System.Drawing.Point(67, 98);
            this.Flabel4.Name = "Flabel4";
            this.Flabel4.Size = new System.Drawing.Size(115, 16);
            this.Flabel4.TabIndex = 8;
            this.Flabel4.Text = "プレイヤー2のログ";
            // 
            // FPlayer1RichTextBox
            // 
            this.FPlayer1RichTextBox.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FPlayer1RichTextBox.Location = new System.Drawing.Point(70, 340);
            this.FPlayer1RichTextBox.Name = "FPlayer1RichTextBox";
            this.FPlayer1RichTextBox.ReadOnly = true;
            this.FPlayer1RichTextBox.Size = new System.Drawing.Size(187, 172);
            this.FPlayer1RichTextBox.TabIndex = 7;
            this.FPlayer1RichTextBox.TabStop = false;
            this.FPlayer1RichTextBox.Text = "";
            // 
            // FPlayer2RichTextBox
            // 
            this.FPlayer2RichTextBox.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FPlayer2RichTextBox.Location = new System.Drawing.Point(70, 117);
            this.FPlayer2RichTextBox.Name = "FPlayer2RichTextBox";
            this.FPlayer2RichTextBox.ReadOnly = true;
            this.FPlayer2RichTextBox.Size = new System.Drawing.Size(186, 172);
            this.FPlayer2RichTextBox.TabIndex = 6;
            this.FPlayer2RichTextBox.TabStop = false;
            this.FPlayer2RichTextBox.Text = "";
            // 
            // FLabel3
            // 
            this.FLabel3.AutoSize = true;
            this.FLabel3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FLabel3.Location = new System.Drawing.Point(0, 273);
            this.FLabel3.Name = "FLabel3";
            this.FLabel3.Size = new System.Drawing.Size(40, 16);
            this.FLabel3.TabIndex = 5;
            this.FLabel3.Text = "手番";
            // 
            // FLabel2
            // 
            this.FLabel2.AutoSize = true;
            this.FLabel2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FLabel2.Location = new System.Drawing.Point(3, 56);
            this.FLabel2.Name = "FLabel2";
            this.FLabel2.Size = new System.Drawing.Size(123, 16);
            this.FLabel2.TabIndex = 4;
            this.FLabel2.Text = "プレイヤー2の得点";
            // 
            // FLabel1
            // 
            this.FLabel1.AutoSize = true;
            this.FLabel1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FLabel1.Location = new System.Drawing.Point(3, 515);
            this.FLabel1.Name = "FLabel1";
            this.FLabel1.Size = new System.Drawing.Size(123, 16);
            this.FLabel1.TabIndex = 3;
            this.FLabel1.Text = "プレイヤー1の得点";
            // 
            // FTurnTextBox
            // 
            this.FTurnTextBox.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FTurnTextBox.Location = new System.Drawing.Point(3, 295);
            this.FTurnTextBox.Name = "FTurnTextBox";
            this.FTurnTextBox.ReadOnly = true;
            this.FTurnTextBox.Size = new System.Drawing.Size(123, 23);
            this.FTurnTextBox.TabIndex = 2;
            this.FTurnTextBox.TabStop = false;
            // 
            // FPlayer2PointTextBox
            // 
            this.FPlayer2PointTextBox.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FPlayer2PointTextBox.Location = new System.Drawing.Point(5, 75);
            this.FPlayer2PointTextBox.Name = "FPlayer2PointTextBox";
            this.FPlayer2PointTextBox.ReadOnly = true;
            this.FPlayer2PointTextBox.Size = new System.Drawing.Size(50, 23);
            this.FPlayer2PointTextBox.TabIndex = 1;
            this.FPlayer2PointTextBox.TabStop = false;
            // 
            // FPlayer1PointTextBox
            // 
            this.FPlayer1PointTextBox.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FPlayer1PointTextBox.Location = new System.Drawing.Point(3, 534);
            this.FPlayer1PointTextBox.Name = "FPlayer1PointTextBox";
            this.FPlayer1PointTextBox.ReadOnly = true;
            this.FPlayer1PointTextBox.Size = new System.Drawing.Size(50, 23);
            this.FPlayer1PointTextBox.TabIndex = 0;
            this.FPlayer1PointTextBox.TabStop = false;
            // 
            // HasamiShogiForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(810, 573);
            this.Controls.Add(this.FSplitContainer1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "HasamiShogiForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "はさみ将棋";
            this.FSplitContainer1.Panel2.ResumeLayout(false);
            this.FSplitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FSplitContainer1)).EndInit();
            this.FSplitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer FSplitContainer1;
        private System.Windows.Forms.TextBox FPlayer2PointTextBox;
        private System.Windows.Forms.TextBox FPlayer1PointTextBox;
        private System.Windows.Forms.TextBox FTurnTextBox;
        private System.Windows.Forms.Label FLabel1;
        private System.Windows.Forms.Label FLabel2;
        private System.Windows.Forms.Label FLabel3;
        private System.Windows.Forms.RichTextBox FPlayer2RichTextBox;
        private System.Windows.Forms.Label Flabel5;
        private System.Windows.Forms.Label Flabel4;
        private System.Windows.Forms.RichTextBox FPlayer1RichTextBox;
        private System.Windows.Forms.Label FLabel6;
        private System.Windows.Forms.TextBox FTurnCountTextBox;
        private System.Windows.Forms.Button FResetButton;
        private System.Windows.Forms.Button FRuleButton;
    }
}

