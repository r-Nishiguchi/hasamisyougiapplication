﻿using System.Drawing;

namespace HasamiShogi {
    /// <summary>
    /// プレイヤーの種類
    /// </summary>
    public enum PlayerKindEnum {
        Player1,
        Player2,
    }
    /// <summary>
    /// 方向
    /// </summary>
    public enum DirectionKindEnum {
        Up,
        Right,
        Down,
        Left,
    }
    /// <summary>
    /// 色やフィールド・得点の上限の定義
    /// </summary>
    public static class Define {
        /// <summary>
        /// 盤面の長さ
        /// </summary>
        public static readonly int C_FieldLength = 9;
        /// <summary>
        /// 駒を取った数の上限
        /// </summary>
        public static readonly int C_MaxGetPoint = 5;
        /// <summary>
        /// 元の色
        /// </summary>
        public static readonly Color C_OriginalColor = Color.SandyBrown;
        /// <summary>
        /// 選択中の色
        /// </summary>
        public static readonly Color C_SelectColor = Color.Lime;
        /// <summary>
        /// 移動可能な場所の色
        /// </summary>
        public static readonly Color C_MovableColor = Color.Yellow;
    }
}
