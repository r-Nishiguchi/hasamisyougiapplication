﻿using System.Drawing;
using System.Windows.Forms;

namespace HasamiShogi {
    /// <summary>
    /// 駒
    /// </summary>
    public class Piece : Button {
        private bool FIsSelected;
        /// <summary>
        /// 選択状態かどうか
        /// </summary>
        public bool IsSelected {
            get => FIsSelected;
            set {
                this.BackColor = (value) ? Define.C_SelectColor : Define.C_OriginalColor;
                FIsSelected = value;
            }
        }
        private bool FIsMovavle;
        /// <summary>
        /// 移動可能かどうか
        /// </summary>
        public bool IsMovavle {
            get => FIsMovavle;
            set {
                this.BackColor = (value) ? Define.C_MovableColor : Define.C_OriginalColor;
                FIsMovavle = value;
            }
        }
        /// <summary>
        /// 取り除ける状態かどうか
        /// </summary>
        public bool IsRemovable { get; set; }
        /// <summary>
        /// プレイヤーの種類
        /// </summary>
        public PlayerKindEnum? PlayerKind { get; set; }
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public Piece() {
            this.Size = new Size(50, 50);
            this.Margin = new Padding(3, 3, 3, 3);
            this.PlayerKind = null;
            this.FlatStyle = FlatStyle.Flat;
            this.FlatAppearance.BorderColor = Color.Black;
            this.FlatAppearance.BorderSize = 2;
            this.TabIndex = 1;
            this.Font = new Font("MS UI Gothic", 15F, FontStyle.Bold);
            this.BackColor = Color.SandyBrown;
        }
        /// <summary>
        /// 初期化
        /// </summary>
        /// <param name="vPlayer">プレイヤーの種類による初期化</param>
        public void InitializePiece(Player vPlayer) {
            this.Enabled = true;
            this.IsRemovable = false;
            this.PlayerKind = vPlayer?.Kind;
            this.ForeColor = vPlayer?.PieceColor ?? Define.C_OriginalColor;
            this.Text = vPlayer?.PieceName;

            vPlayer?.MyPiecies.Add(this);
        }
    }
}
