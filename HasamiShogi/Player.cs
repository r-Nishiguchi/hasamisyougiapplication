﻿using System.Collections.Generic;
using System.Drawing;

namespace HasamiShogi {
    /// <summary>
    /// プレイヤー
    /// </summary>
    public class Player {
        /// <summary>
        /// 持ち駒リスト
        /// </summary>
        public List<Piece> MyPiecies { get; set; } = new List<Piece>();
        /// <summary>
        /// プレイヤーの名前
        /// </summary>
        public string Name { get; }
        /// <summary>
        /// 駒の名前
        /// </summary>
        public string PieceName { get; }
        /// <summary>
        /// プレイヤーの種類
        /// </summary>
        public PlayerKindEnum Kind { get; }
        /// <summary>
        /// 駒の色
        /// </summary>
        public Color PieceColor { get; }
        /// <summary>
        /// プレイヤーの得点
        /// </summary>
        public int Point { get; set; }
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public Player(string vName,PlayerKindEnum vKind,Color vColor,string vPieceName) {
            this.Name = vName;
            this.Kind = vKind;
            this.PieceColor = vColor;
            this.PieceName = vPieceName;
        }
        /// <summary>
        /// セッタープロパティを初期化
        /// </summary>
        public void Initialize() {
            this.Point = 0;
            this.MyPiecies.RemoveAll(n => n.PlayerKind != null);
        }
    }
}
