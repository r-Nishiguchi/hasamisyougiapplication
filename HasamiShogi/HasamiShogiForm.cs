﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace HasamiShogi {
    /// <summary>
    /// はさみ将棋のプレイ画面
    /// </summary>
    public partial class HasamiShogiForm : Form {
        #region フィールド
        private GamaManager FGameManager;
        #endregion
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public HasamiShogiForm() {

            InitializeComponent();
            FGameManager = new GamaManager();
            //盤面を生成
            for (int i = 0; i < Define.C_FieldLength; i++) {
                for (int j = 0; j < Define.C_FieldLength; j++) {
                    Piece wPiece = new Piece();
                    wPiece.Location = new Point(j * 50 + 50, i * 50 + 50);
                    wPiece.Click += new EventHandler(ButtonClick);
                    FGameManager.AddBoard(wPiece, (i, j));
                    FSplitContainer1.Panel1.Controls.Add(wPiece);
                }
            }
            //ラベル生成
            for (int i = 0; i < Define.C_FieldLength; i++) {
                Label wXLabel = new Label();
                wXLabel.Location = new Point(i * 50 + 70, 40);
                wXLabel.Size = new Size(10, 10);
                wXLabel.Text = $"{i + 1}";
                wXLabel.TextAlign = ContentAlignment.MiddleCenter;
                FSplitContainer1.Panel1.Controls.Add(wXLabel);
            }
            //ラベル生成
            for (int i = 0; i < Define.C_FieldLength; i++) {
                Label wYLabel = new Label();
                wYLabel.Location = new Point(500, i * 50 + 70);
                wYLabel.Size = new Size(10, 10);
                wYLabel.Text = $"{i + 1}";
                wYLabel.TextAlign = ContentAlignment.MiddleCenter;
                FSplitContainer1.Panel1.Controls.Add(wYLabel);
            }
            //「歩」と「と金」を設定
            FGameManager.Initialize();
            ShowLog();
        }
        /// <summary>
        /// ボタンクリック
        /// </summary>
        /// <param name="sender">ボタン</param>
        /// <param name="e">クリック</param>
        private void ButtonClick(object sender, EventArgs e) {
            Piece wPiece = (Piece)sender;
            FGameManager.SelectPiece(wPiece);
            FGameManager.MovePiece(wPiece);
            ShowLog();
        }
        /// <summary>
        /// ログの表示
        /// </summary>
        private void ShowLog() {
            Log wPalyerLog = FGameManager.GetPlayerLog();
            FPlayer1PointTextBox.Text = wPalyerLog.Player1PointLog;
            FPlayer2PointTextBox.Text = wPalyerLog.Player2PointLog;
            FPlayer1RichTextBox.Text = wPalyerLog.Player1ActionLog;
            FPlayer2RichTextBox.Text = wPalyerLog.Player2ActionLog;
            FTurnCountTextBox.Text = wPalyerLog.TurnCountLog.ToString();
            FTurnTextBox.Text = wPalyerLog.TurnPlayerLog;
        }
        /// <summary>
        /// リセット機能
        /// </summary>
        /// <param name="sender">リセットボタン</param>
        /// <param name="e">クリック</param>
        private void FResetButton_Click(object sender, EventArgs e) {
            FGameManager.ResetGame();
            ShowLog();
        }
        /// <summary>
        /// ルール表示機能
        /// </summary>
        /// <param name="sender">ルールボタン</param>
        /// <param name="e">クリック</param>
        private void FRuleButton_Click(object sender, EventArgs e) {
            using (RuleForm wRuleForm = new RuleForm()) {
                wRuleForm.ShowDialog(this);
            }
        }
    }
}

