﻿using System;

namespace HasamiShogi {
    /// <summary>
    /// プレイヤー管理
    /// </summary>
    public class PlayerManager {
        /// <summary>
        /// ターン数
        /// </summary>
        public int TurnCount { get; private set; }
        /// <summary>
        /// 他のプレイヤー
        /// </summary>
        public Player AnotherPlayer { get; private set; }
        /// <summary>
        /// 現在のプレイヤー
        /// </summary>
        public Player CurrentPlayer { get; private set; }
        /// <summary>
        /// 3点差あるかどうか
        /// </summary>
        public bool IsThrrePointGap => Math.Abs(this.CurrentPlayer.Point - this.AnotherPlayer.Point) >= 3;
        /// <summary>
        /// プレイヤーのログ
        /// </summary>
        public Log PlayerLog { get;}
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="vPlayer1">Player1のインスタンス</param>
        /// <param name="vPlayer2">Player2のインスタンス</param>
        public PlayerManager(Player vPlayer1,Player vPlayer2) {
            InitializePlayerManager(vPlayer1, vPlayer2);
            this.PlayerLog = new Log(vPlayer1.Name);
        }
        /// <summary>
        /// プレイヤーを切り替える
        /// </summary>
        public void SwitchPlayer() {
            Player wCurrentPlayer = this.CurrentPlayer;
            this.CurrentPlayer = this.AnotherPlayer;
            this.AnotherPlayer = wCurrentPlayer;

            this.TurnCount += 1;
            this.PlayerLog.TurnPlayerLog = this.CurrentPlayer.Name;
            this.PlayerLog.TurnCountLog = this.TurnCount;
        }
        /// <summary>
        /// プレイヤー設定の初期化
        /// </summary>
        /// <param name="vPlayer1">プレイヤー1</param>
        /// <param name="vPlayer2">プレイヤー2</param>
        public void InitializePlayerManager(Player vPlayer1, Player vPlayer2) {
            this.TurnCount = 0;
            this.CurrentPlayer = vPlayer1;
            this.AnotherPlayer = vPlayer2;
        }
        /// <summary>
        /// 終了判定
        /// </summary>
        /// <param name="vIsThreePointGap">3点差かどうか</param>
        /// <returns>決着しているかどうか</returns>
        public bool IsEnd(bool vIsThreePointGap) {
            //5枚駒を取れば終了
            if (this.CurrentPlayer.Point >= Define.C_MaxGetPoint) return true;
            //既に3点以上の差があり、駒を取り除いた後に差を縮められなければtrue
            if (vIsThreePointGap) return (this.IsThrrePointGap);
            return false;
        }
        /// <summary>
        /// 勝者を取得する
        /// </summary>
        /// <returns></returns>
        public Player GetWinner() => (this.CurrentPlayer.Point > this.AnotherPlayer.Point) ? this.CurrentPlayer : this.AnotherPlayer;
    }
}
